const fs = require('fs')

const variation = process.argv[2]

const getFileData = (fileName) => {
  try {
    const dataBuffer = fs.readFileSync(fileName)
    const dataJSON = dataBuffer.toString()
    return JSON.parse(dataJSON)
  } catch(err) {
    throw new Error(`could not read ${fileName} file`)
  }
}

const followers = getFileData('followers.json').map(data => data.string_list_data[0].value)
const followings = getFileData('following.json').relationships_following.map(data => data.string_list_data[0].value)

let list
if (variation === 'followings') {
  list = followers.filter(username => !followings.includes(username))
} else {
  list = followings.filter(username => !followers.includes(username))
}

const jsonList = JSON.stringify(list, null, 2); // 'null, 2' adds formatting

fs.writeFile('file.json', jsonList, (err) => {
  if (err) {
    console.error('Error writing file:', err);
  } else {
    console.log('Array successfully written to file.json');
  }
});

// console.table(list)
// console.dir(list, { depth: null, maxArrayLength: null });
